package com.parkinglot;

public class ParkingTicket {
    private int ticketId;
    private boolean used;

    public boolean isUsed() {
        return used;
    }

    public void setUsed(boolean used) {
        this.used = used;
    }

//    public ParkingTicket(int ticketId, boolean used) {
//        this.ticketId = ticketId;
//        this.used = used;
//    }


    public ParkingTicket(int ticketId) {
        this.ticketId = ticketId;
    }

    public int getTicketId() {
        return ticketId;
    }

    public void setTicketId(int ticketId) {
        this.ticketId = ticketId;
    }
}
