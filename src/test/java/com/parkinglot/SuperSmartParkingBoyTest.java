package com.parkinglot;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class SuperSmartParkingBoyTest {

    @Test
    void should_return_parkinglot2_ticket_when_park_given_superSmartParkingBoy_and_two_available_parkingLots() {
     //given
        Car car = new Car(0);

        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);

        parkingLot1.setCapacity(2);
        parkingLot2.setCapacity(3);

        parkingLot1.park(new Car(1));
        parkingLot2.park(new Car(2));

        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLots);
        ParkingTicket ticket = superSmartParkingBoy.helpPark(car);
        //when

     //then
        assertFalse( parkingLot2.getCarList().contains(car));
        assertTrue( parkingLot1.getCarList().contains(car));

    }
    @Test
    void should_return_parkinglot2_ticket_when_park_given_superSmartParkingBoy_and_full_parkingLot1_and_avaiable_parkingLot2() {
        //given
        Car car = new Car(20);

        ParkingLot parkingLot1 = new ParkingLot();
        for(int i=0;i<parkingLot1.getCapacity();i++){
            parkingLot1.park(new Car(i));
        }
        ParkingLot parkingLot2 = new ParkingLot();
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLots);
        ParkingTicket ticket = superSmartParkingBoy.helpPark(car);
        //when

        //then
        assertFalse( parkingLot1.getCarList().contains(car));
        assertTrue( parkingLot2.getCarList().contains(car));

    }
    @Test
    void should_return_right_car_when_fetch_given_two_parkingLots_and_superSmartParkingBoy_and_parked_car_with_tickets() {
        //given
        Car car = new Car(4);
        Car car1 = new Car(5);

        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLots);
        ParkingTicket ticket = superSmartParkingBoy.helpPark(car);
        ParkingTicket ticket1 = superSmartParkingBoy.helpPark(car1);
        //when
        Car rightCar = superSmartParkingBoy.helpFetch(ticket);
        Car normalCar = superSmartParkingBoy.helpFetch(ticket1);
        //then
       assertEquals(rightCar, car);
       assertEquals(normalCar, car1);
    }
    @Test
    void should_throw_unrecongnizedException_when_fetch_given_superSmartParkingBoy_and_two_parkingLots_and_unrecongnized_ticket() {
        //given
        Car car = new Car(4);

        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLots);
        superSmartParkingBoy.helpPark(car);
        //when

        //then
        var exception = assertThrows(UnRecongnizedPakingTiketException.class, () -> superSmartParkingBoy.helpFetch(new ParkingTicket(5)));
        assertEquals("Unrecongized Paking ticket", exception.getMessage());
    }
    @Test
    void should_throw_unrecongnizedException_when_fetch_given_superSmartParkingBoy_and_two_parkingLots_and_used_ticket() {
        //given

        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLots);

        ParkingTicket usedTick = superSmartParkingBoy.helpPark(new Car(4));
        superSmartParkingBoy.helpFetch(usedTick);
        //then
      var exeptions = assertThrows(UnRecongnizedPakingTiketException.class, ()-> superSmartParkingBoy.helpFetch(usedTick));
      assertEquals("Unrecongized Paking ticket", exeptions.getMessage());
    }
//
    @Test
    void should_throw_exception_when_park_given_car_and_full_parkingLots() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLots);

        for(int i=0;i<10;i++){
            parkingLot1.park(new Car(i));
            parkingLot2.park(new Car(i+10));
        }

        //when

        var exception = assertThrows(NoAvailableException.class, () -> superSmartParkingBoy.helpPark(new Car(11)));

        //then
        assertEquals("No available position.", exception.getMessage());
    }

}
