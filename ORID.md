## ORID

Objective： 
 Today, we first conducted a code review. For yesterday's homework, I found the team members' good ideas to solve the problem. Through everyone's solution, I also have a preliminary understanding of the strategy model. Then we learned the requirement about parking lot. This example is used to practice TDD and OO. Drill down into TDD and OO practices by increasing the complexity of requirements. 

Reflective：
 When the requirements become complex, it is a little difficult to implement

 Interpretive：
 I feel that the implementation process is difficult because my foundation is still relatively weak, and it is not smooth when programming with syntax.

 Decisional：
 I will use the extra time on my own to make up and practice the weak parts. 