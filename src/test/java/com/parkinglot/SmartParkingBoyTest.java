package com.parkinglot;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class SmartParkingBoyTest {

    @Test
    void should_return_parkinglot2_ticket_when_park_given_smartParkingBoy_and_two_available_parkingLots() {
     //given
        Car car = new Car(0);

        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        parkingLot1.park(new Car(1));

        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLots);
        smartParkingBoy.helpPark(car);
        //when

     //then
        assertFalse( parkingLot1.getCarList().contains(car));
        assertTrue( parkingLot2.getCarList().contains(car));
    }
    @Test
    void should_return_parkinglot2_ticket_when_park_given_smartParkingBoy_and_full_parkingLot1_and_avaiable_parkingLot2() {
        //given
        Car car = new Car(20);

        ParkingLot parkingLot1 = new ParkingLot();
        for(int i=0;i<parkingLot1.getCapacity();i++){
            parkingLot1.park(new Car(i));
        }
        ParkingLot parkingLot2 = new ParkingLot();
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLots);
        smartParkingBoy.helpPark(car);
        //when

        //then
        assertFalse( parkingLot1.getCarList().contains(car));
        assertTrue( parkingLot2.getCarList().contains(car));

    }
    @Test
    void should_return_right_car_when_fetch_given_two_parkingLots_and_smartParkingBoy_and_parked_car_with_tickets() {
        //given
        Car car = new Car(4);
        Car car1 = new Car(5);

        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLots);
        ParkingTicket ticket = smartParkingBoy.helpPark(car);
        ParkingTicket ticket1 = smartParkingBoy.helpPark(car1);
        //when
        Car rightCar = smartParkingBoy.helpFetch(ticket);
        Car normalCar = smartParkingBoy.helpFetch(ticket1);
        //then
       assertEquals(rightCar, car);
        assertEquals(normalCar, car1);
    }
    @Test
    void should_throw_unrecongnizedException_when_fetch_given_smartParkingBoy_and_two_parkingLots_and_unrecongnized_ticket() {
        //given
        Car car = new Car(4);

        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLots);
        smartParkingBoy.helpPark(car);
        //when

        //then
        var exception = assertThrows(UnRecongnizedPakingTiketException.class, () -> smartParkingBoy.helpFetch(new ParkingTicket(5)));
        assertEquals("Unrecongized Paking ticket", exception.getMessage());
    }
    @Test
    void should_throw_unrecongnizedException_when_fetch_given_smartParkingBoy_and_two_parkingLots_and_used_ticket() {
        //given
        Car car = new Car(4);

        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLots);

        ParkingTicket usedTick = smartParkingBoy.helpPark(new Car(4));
        smartParkingBoy.helpFetch(usedTick);
        //then
      var exeptions = assertThrows(UnRecongnizedPakingTiketException.class, ()-> smartParkingBoy.helpFetch(usedTick));
      assertEquals("Unrecongized Paking ticket", exeptions.getMessage());
    }

    @Test
    void should_throw_exception_when_park_given_car_and_full_parkingLots() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLots);

        for(int i=0;i<10;i++){
            parkingLot1.park(new Car(i));
            parkingLot2.park(new Car(i+10));
        }

        //when

        var exception = assertThrows(NoAvailableException.class, () -> smartParkingBoy.helpPark(new Car(11)));

        //then
        assertEquals("No available position.", exception.getMessage());
    }

}
