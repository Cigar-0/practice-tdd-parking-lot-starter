package com.parkinglot;

import java.util.ArrayList;
import java.util.List;

public class ParkingBoy {
    List<ParkingLot> parkingLotList = new ArrayList<>();

    public ParkingBoy(List<ParkingLot> parkingLots) {
        parkingLotList.addAll(parkingLots);
    }
    public ParkingTicket helpPark(Car car) {
        return parkingLotList.stream().filter(ParkingLot::isHasPosition).findFirst().map(parkingLot -> parkingLot.park(car)).orElseThrow(NoAvailableException::new);
    }
    public Car helpFetch(ParkingTicket ticket) {
        return parkingLotList.stream().filter(parkingLot -> parkingLot.isParkedIn(ticket)).findFirst().map(parkingLot -> parkingLot.fetch(ticket)).orElseThrow(UnRecongnizedPakingTiketException::new);
    }
}
