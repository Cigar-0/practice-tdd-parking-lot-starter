package com.parkinglot;

public class NoAvailableException extends RuntimeException{
    public NoAvailableException() {
        super("No available position.");
    }
}
