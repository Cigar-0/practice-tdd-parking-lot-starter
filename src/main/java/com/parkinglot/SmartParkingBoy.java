package com.parkinglot;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class SmartParkingBoy {
    List<ParkingLot> parkingLotList = new ArrayList<>();

    public SmartParkingBoy(List<ParkingLot> parkingLots) {
        parkingLotList.addAll(parkingLots);
    }
    public ParkingTicket helpPark(Car car) {
        return parkingLotList.stream().filter(ParkingLot::isHasPosition).max(Comparator.comparingInt(parkingLot->(parkingLot.getCapacity() - parkingLot.getCarList().size()))).map(parkingLot -> parkingLot.park(car)).orElseThrow(NoAvailableException::new);
    }
    public Car helpFetch(ParkingTicket ticket) {
        return parkingLotList.stream().filter(parkingLot -> parkingLot.isParkedIn(ticket)).findFirst().map(parkingLot -> parkingLot.fetch(ticket)).orElseThrow(UnRecongnizedPakingTiketException::new);
    }
}
