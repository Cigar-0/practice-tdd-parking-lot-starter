package com.parkinglot;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingLotTest {
    @Test
    void should_return_ticket_when_park_given_parkingLot_and_car() {
     //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car(4);

        ParkingTicket ticket = parkingLot.park(car);

        //when

     //then
        assertNotNull(ticket);
    }
    @Test
    void should_return_car_when_fetch_given_parkingLot_and_ticket() {
     //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car(4);
        ParkingTicket ticket = parkingLot.park(car);
        Car fetchedCar = parkingLot.fetch(ticket);
        //when

     //then
        assertEquals(car, fetchedCar);
    }
//    @Test
//    void should_return_null_when_park_given_car_and_full_parkingLot() {
//     //given
//        Car car = new Car(3);
//        ParkingLot parkingLot = new ParkingLot();
//        for(int i=0;i<10;i++){
//            parkingLot.park(new Car(i));
//        }
//
//
//        //when
//        ParkingTicket ticket = parkingLot.park(new Car(11));
//
//     //then
//        assertNull(ticket);
//    }
//    @Test
//    void should_return_null_when_fetch_given_wrong_ticket() {
//     //given
//        ParkingTicket wrongTicket = new ParkingTicket(2);
//        ParkingLot parkingLot = new ParkingLot();
//        wrongTicket.setTicketId(0);
//
//
//        //when
//
//        Car fetchedCar = parkingLot.fetch(wrongTicket);
//
//     //then
//        assertNull(fetchedCar);
//    }
    @Test
    void should_return_right_car_when_fetch_given_two_cars() {
     //given
        ParkingLot parkingLot = new ParkingLot();
        Car car1 = new Car(1);
        Car car2 = new Car(2);
        ParkingTicket ticket1 = parkingLot.park(car1);
        ParkingTicket ticket2 = parkingLot.park(car2);

        //when
        Car fetchedCar1 = parkingLot.fetch(ticket1);
        Car fetchedCar2 = parkingLot.fetch(ticket2);
     
     //then
        assertEquals(car1, fetchedCar1);
        assertEquals(car2, fetchedCar2);
    }
//    @Test
//    void should_return_null_when_fetch_given_used_ticket() {
//        //given
//        ParkingLot parkingLot = new ParkingLot();
//        Car car1 = new Car(1);
//        ParkingTicket ticket1 = parkingLot.park(car1);
//
//
//        //when
//        parkingLot.fetch(ticket1);
//        Car fetchedCar = parkingLot.fetch(ticket1);
//
//        //then
//        assertNull(fetchedCar);
//    }
    @Test
    void should_throw_exception_with_error_message_when_fetch_given_parkingLot_and_car_used() {
        //given
        ParkingTicket wrongTicket = new ParkingTicket(1);
        ParkingLot parkingLot = new ParkingLot();


        //when
        ParkingTicket usedTick = parkingLot.park(new Car(4));
        parkingLot.fetch(usedTick);

        //then
        var exeptions = assertThrows(UnRecongnizedPakingTiketException.class, ()-> parkingLot.fetch(usedTick));

        assertEquals("Unrecongized Paking ticket",exeptions.getMessage());
    }
    @Test
    void should_throw_exception_with_error_message_when_fetch_given_parkingLot_and_wrong_ticket() {
        ParkingLot parkingLot = new ParkingLot();

        parkingLot.park(new Car(4));
        var exception = assertThrows(UnRecongnizedPakingTiketException.class, () -> parkingLot.fetch(new ParkingTicket(5)));

        assertEquals("Unrecongized Paking ticket", exception.getMessage());
    }
    @Test
    void should_throw_exception_when_park_given_car_and_full_parkingLot() {
        //given
        Car car = new Car(3);
        ParkingLot parkingLot = new ParkingLot();
        for(int i=0;i<10;i++){
            parkingLot.park(new Car(i));
        }


        //when

        var exception = assertThrows(NoAvailableException.class, () -> parkingLot.park(new Car(11)));

        assertEquals("No available position.", exception.getMessage());

        //then

    }


}
