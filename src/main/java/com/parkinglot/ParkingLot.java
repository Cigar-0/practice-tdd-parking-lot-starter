package com.parkinglot;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ParkingLot {
    private int capacity=10;

    public List<Car> getCarList() {
        return carList;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    List<Car> carList = new ArrayList<>();

    public boolean isHasPosition() {
        return !(carList.size() >= capacity);
    }
    public boolean isParkedIn(ParkingTicket parkingTicket) {
        return carList.stream().anyMatch(car -> car.getId() == parkingTicket.getTicketId());
    }

    public ParkingTicket park(Car car) {
        if(carList.size() >= capacity){
            throw new NoAvailableException();
        }else {
            carList.add(car);
            return new ParkingTicket(car.getId());
        }
    }


    public Car fetch(ParkingTicket ticket) {
        List<Car> collect = carList.stream().filter(car -> car.getId() == ticket.getTicketId()).collect(Collectors.toList());

        if(collect.size() == 0 || ticket.isUsed() ){
            throw new UnRecongnizedPakingTiketException();
        }else {
            Car tmpCar =  collect.get(0);
            ticket.setUsed(true);
            carList.remove(0);
            return tmpCar;

        }
    }

    public int getCapacity() {
        return capacity;
    }
}
