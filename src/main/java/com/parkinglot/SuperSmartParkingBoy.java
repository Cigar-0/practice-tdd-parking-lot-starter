package com.parkinglot;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class SuperSmartParkingBoy {
    List<ParkingLot> parkingLotList = new ArrayList<>();

    public SuperSmartParkingBoy(List<ParkingLot> parkingLots) {
        parkingLotList.addAll(parkingLots);
    }
    public ParkingTicket helpPark(Car car) {
        return parkingLotList.stream().filter(ParkingLot::isHasPosition).max(Comparator.comparingInt(o -> (o.getCapacity() - o.getCarList().size()) / o.getCapacity())).map(parkingLot -> parkingLot.park(car)).orElseThrow(NoAvailableException::new);
    }
    public Car helpFetch(ParkingTicket ticket) {
        return parkingLotList.stream().filter(parkingLot -> parkingLot.isParkedIn(ticket)).findFirst().map(parkingLot -> parkingLot.fetch(ticket)).orElseThrow(UnRecongnizedPakingTiketException::new);
    }
}
